def call(Map config = [:]){
   sh """
     mvn -U -DXmx4096m clean install -P ${config.ENV} -Dmaven.test.skip=true
   """
}