// Wechat
// <font color="info">绿色</font>
// <font color="comment">灰色</font>
// <font color="warning">橙红色</font>
def call(Map config=[:]){
    sh """
        curl -X POST ${config.WEBHOOK} \
        -H 'Content-Type: application/json; charset=utf-8' \
        -d '{"msgtype": "markdown","markdown": \
        {"content": "URL: [${env.JOB_NAME}](${env.BUILD_URL})
        > Branch: ${env.BRANCH_NAME}
        > BuilderNumber: ${currentBuild.number}
        > BuildResult: <font color='${config.COLOR}'>${config.RESULT}</font>
        > BuildDuration: ${currentBuild.durationString}"
        }}'
    """
}