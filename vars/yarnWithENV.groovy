def call(Map config = [:]) {
    sh """
        yarn config set registry ${config.REGITRY}
        yarn install
        npm run build:${config.ENV}
    """
}