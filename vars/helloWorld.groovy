// def call() {
//     sh 'echo Hello World'
// }

//def call(String name, String dayOfWeek) {
//    sh "echo Hello ${name}. Today is ${dayOfWeek}."
//}

// create a null map [key:value] and define map key.
def call(Map config = [:]) {
    sh "echo Hello ${config.name}. Today is ${config.dayOfWeek}."
}
